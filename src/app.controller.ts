import { Body, Controller, Get, Param, ParseIntPipe, Post, UsePipes } from '@nestjs/common';
import { title } from 'process';
import { AppService } from './app.service';
import { SlugPipe } from './slug.pipe';
import { UpperPipe } from './upper.pipe';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':name')
  @UsePipes(UpperPipe)
  getHello(@Param('name') name: string): string {
    return this.appService.getHello(name);
  }

  @Get('articles/:id')
  @UsePipes(ParseIntPipe)
  getArticleById(@Param('id') id) {
    const idType = typeof id;
    const res = { id: id, idType: idType};
    return res;
  }

  @Post('article')
  @UsePipes(SlugPipe)
  createArticle(@Body('title') title, @Body() allBody) {
     allBody.slug = title;
     return allBody;
  }


  @Post()
  @UsePipes(UpperPipe)
  createMessage(@Body() message) {
    return message;
  }
}
